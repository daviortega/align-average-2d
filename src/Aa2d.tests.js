'use strict'

const path = require('path')
const fs = require('fs')

const expect = require('chai').expect

const savePixels = require('save-pixels')
const show = require('ndarray-show')
const pack = require('ndarray-pack')
const convolve = require('ndarray-convolve')
const ops = require('ndarray-ops')
const Aa2d = require('./Aa2d')
const zeros = require('zeros')
const imageRotate = require('image-rotate')

const fileStack = [
	path.resolve(__dirname, '../test-data/image1.png'),
	path.resolve(__dirname, '../test-data/image2.png'),
	path.resolve(__dirname, '../test-data/image3.png'),
	path.resolve(__dirname, '../test-data/image4.png'),
	path.resolve(__dirname, '../test-data/image5.png'),
	path.resolve(__dirname, '../test-data/image6.png'),
	path.resolve(__dirname, '../test-data/image7.png')
]

const smallFileStack = [
	path.resolve(__dirname, '../test-data/small1.png'),
	path.resolve(__dirname, '../test-data/small2.png'),
	path.resolve(__dirname, '../test-data/small3.png'),
	path.resolve(__dirname, '../test-data/small4.png'),
	path.resolve(__dirname, '../test-data/small5.png')
]

describe('align-average-2d', function() {
	it('must load stacks of images', function() {
		const expected = [
			{
				format: 'raw',
				width: 84,
				height: 84,
				channels: 4,
				premultiplied: false,
				size: 28224
			},
			{
				format: 'raw',
				width: 84,
				height: 84,
				channels: 4,
				premultiplied: false,
				size: 28224
			},
			{
				format: 'raw',
				width: 84,
				height: 84,
				channels: 4,
				premultiplied: false,
				size: 28224
			},
			{
				format: 'raw',
				width: 94,
				height: 91,
				channels: 4,
				premultiplied: false,
				size: 34216
			},
			{
				format: 'raw',
				width: 94,
				height: 91,
				channels: 4,
				premultiplied: false,
				size: 34216
			},
			{
				format: 'raw',
				width: 91,
				height: 90,
				channels: 4,
				premultiplied: false,
				size: 32760
			},
			{
				format: 'raw',
				width: 100,
				height: 100,
				channels: 4,
				premultiplied: false,
				size: 40000
			}
		]
		const aa2d = Aa2d()
		return aa2d.loadStack(fileStack)
			.then(() => {
				const results = []
				aa2d.imageStack.forEach((image) => {
					results.push(image.info)
				})
				return results
			})
			.then((results) => {
				expect(results).eql(expected)
			})
	})
	describe('crosscor', function() {
		it('should be 1 if same image', function() {
			const aa2d = Aa2d()
			const imageIndex = 0
			return aa2d.loadStack(fileStack)
				.then(() => {
					const coef = aa2d.crosscor(imageIndex, imageIndex + 2)
					console.log(coef.shape)
					console.log(show(coef))
					const write = fs.createWriteStream(
						path.resolve(__dirname, '../test-data/ccorr.png')
					)
					savePixels(coef, 'png').pipe(write)
				})
		})
	})
	describe('convolve', function() {
		it('should do', function() {
			const img1 = pack([
				[0, 0, 0, 0, 0],
				[0, 1, 1, 0, 0],
				[0, 1, 1, 0, 0],
				[0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0]
			])
			const img2 = pack([
				[0, 0, 0, 0, 0],
				[0, 1, 1, 0, 0],
				[0, 1, 1, 0, 0],
				[0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0]
			])
			const coef = zeros([9, 9])
			console.log(show(img1))
			console.log('---')
			convolve.correlate(coef, img1, img2)
			console.log(show(img2))
			console.log('---')
			ops.mulseq(coef, 225)
			console.log(show(coef))
			const write = fs.createWriteStream(
				path.resolve(__dirname, '../test-data/ccorr.png')
			)
			savePixels(coef, 'png').pipe(write)
		})
		it.only('should do', function() {
			const img1 = pack([
				[1, 1],
				[1, 1]
			])
			const img2 = pack([
				[1, 2],
				[3, 4]
			])
			const coef = zeros([3, 3])
			console.log(show(img1))
			console.log('---')
			console.log(show(img2))
			console.log('---')
			convolve.correlate(coef, img1, img2)
			// ops.mulseq(coef, 225)
			// console.log(show(coef))
			const write = fs.createWriteStream(
				path.resolve(__dirname, '../test-data/ccorr.png')
			)
			console.log(show(coef))
			console.log('---')
			imageRotate(coef, coef, Math.PI)
			console.log(show(coef))
			savePixels(coef, 'png').pipe(write)
		})
		it('should do', function() {
			const img1 = pack([
				[1, 1, 1],
				[1, 1, 1]
			])
			const img2 = pack([
				[1, 2],
				[3, 4],
				[5, 6]
			])
			const coef = zeros([4, 4])
			console.log(show(coef))
			console.log('---')
			console.log(show(img1))
			console.log('---')
			console.log(show(img2))
			convolve.correlate(coef, img1, img2)
			console.log('---')
			// ops.mulseq(coef, 225)

			const write = fs.createWriteStream(
				path.resolve(__dirname, '../test-data/ccorr.png')
			)
			imageRotate(coef, coef, Math.PI)
			console.log(show(coef))
			savePixels(coef, 'png').pipe(write)
		})
	})
})
