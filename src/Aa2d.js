'use strict'

const sharp = require('sharp')
const ndarray = require('ndarray')
const ndarrayConvolve = require('ndarray-convolve')


module.exports = (args) => {
	const imageStack = []

	/**
	 * Load a series of images.
	 * 
	 * @param {array} [files=[]] 
	 * @returns Promise
	 */
	const loadStack = (files = []) => {
		const promises = []
		files.forEach((file) => {
			promises.push(new Promise((resolve, reject) => {
				sharp(file)
					.raw()
					.toBuffer({resolveWithObject: true})
					.then((data) => {
						resolve(data)
					})
					.catch((err) => {
						reject(err)
					})
			}))
		})
		return Promise.all(promises).then((data) => {
			data.forEach((img) => {
				const info = img.info
				const rawData = img.data
				const pixels = ndarray(
					new Int8Array(
						img.data
					),
					[
						img.info.width,
						img.info.height
					]
				)
				imageStack.push({
					info,
					data: rawData,
					pixels
				})
			})
		})
	}

	/**
	 * Calculates the cross-correlation coeficient between two images in the stack.
	 * 
	 * @param {integer} i index of first image
	 * @param {integer} j index of the second image
	 */
	const crosscor = (i, j) => {
		const img1 = imageStack[i].pixels
		const img2 = imageStack[j].pixels
		const coef = []
		ndarrayConvolve(img1, img1, img2)
		return img1
	}

	const correlate = (img1, img2) => {
		return null
	}

	return Object.freeze({
		imageStack,
		loadStack,
		crosscor
	})
}
